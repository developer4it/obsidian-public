# Developer4IT 2022-09-09

## Business Roles

  
    _figuur 1_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-236573a21ec1413aa439bb83bfc5e625.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-236573a21ec1413aa439bb83bfc5e625.png)  

## Business Roles + Application Services

  
    _figuur 2_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-ad7387662e0f46fdb58118652c5ac680.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-ad7387662e0f46fdb58118652c5ac680.png)  

## Business Roles + Devices

  
    _figuur 3_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-c0a597b37e9a4bc1a6a497d0bca9143b.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-c0a597b37e9a4bc1a6a497d0bca9143b.png)  

## Business Processes

  
    _figuur 4_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-2d4c045dac8c4989a5c90427fb17c4d7.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-2d4c045dac8c4989a5c90427fb17c4d7.png)  

## Business Processes : generate CV's

  
    _figuur 5_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-d68cfedf3ae7471a9ebdb12109ba3201.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-d68cfedf3ae7471a9ebdb12109ba3201.png)  
Execution of the batch file M:\archi\cv\generate cv's.bat delivers the file M:\cv\cvLarge\CV-Paul-Kok-en.xml.RECOMPOSED-EN.XML. This file can be opened with Word. With Word this can be exported to a large CV in englisch as PDF file.  
  
Execution of the batch file M:\archi\cv\generate cv's.bat delivers the file M:\cv\cvSmall\CV-Paul-Kok-short-en.xml.RECOMPOSED-NL.xml. This file can be opened with Word. With Word this can be exported to a large CV in dutch as PDF file.

## Devices - Data Objects : SAN-SERVER

  
    _figuur 6_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-05d43f7ef8654ae6b4c4cb2d22550936.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-05d43f7ef8654ae6b4c4cb2d22550936.png)  
De windows mapping Volume 1/SAN-SERVER/ARTIFACTS-FROM-JENKINS op de verschillende apparaten wijst naar J:\ARTIFACTS-FROM-JENKINS. The mapping J:\ARTIFACTS-FROM-JENKINS acts as a software repository.

## Devices : SAN-SERVER server - Details

  
    _figuur 7_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-c058bafd600b46baa10e72b58a84946f.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-c058bafd600b46baa10e72b58a84946f.png)  

## Devices : SAN-SERVER server

  
    _figuur 8_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-bb2995fff23d499496dc710843b7b484.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-bb2995fff23d499496dc710843b7b484.png)  

## Applcation Services for Data Objects

  
    _figuur 9_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-f3afa372b96f492bbf2ff7af3340addd.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-f3afa372b96f492bbf2ff7af3340addd.png)  

## Application Services + Devices

  
    _figuur 10_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-326e3390bcce41dda4fec05e5c1e5a0c.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-326e3390bcce41dda4fec05e5c1e5a0c.png)  
The service account.synology.com en gitlab.com kunnen gebruikt worden door alle apparaten van Developer4IT

## Application Services + Devices + Goals

  
    _figuur 11_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-855064e015b04fe8befd81336318e4c6.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-855064e015b04fe8befd81336318e4c6.png)  
The service https://jumpcloud.com/ can be used for the SAN-SERVER server to use LDAP. The service account.synology.com is used for the configuration of the synology account of Developer4IT.

## Artifacts

  
    _figuur 12_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-31b3a841ea48471eb4aea516314da880.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-31b3a841ea48471eb4aea516314da880.png)  

## Artifacts : Firebase + alternatives

Sources:  
  
https://www.youtube.com/watch?v=SXmYUalHyYk  
  
  
  
  
    _figuur 13_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-a271a9602d83460bacbf82ea9e08e45b.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-a271a9602d83460bacbf82ea9e08e45b.png)  
Firebase has several Firebase alternatives.  
  
- AWS Aplify  
  

## Artifacts : Icescrum

  
    _figuur 14_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-8605fd98f04847ea88aab10c290c2c5f.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-8605fd98f04847ea88aab10c290c2c5f.png)  
Icescrum is use to realise the planning and execution of projects.

## Artifacts : Postman + alternatives

Sources:  
  
https://stackshare.io/stackups/hoppscotch-vs-postman  
  
  
  
  
    _figuur 15_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-0a5ea94d61854d8c94deebf9e9ca5a57.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-0a5ea94d61854d8c94deebf9e9ca5a57.png)  
Postman has several Postman alternatives.  
  
- Hoppscotch  
  

## Artifacts : Powerpoint

  
    _figuur 16_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-70c4f880f4be460da9f2f53a3fd31634.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-70c4f880f4be460da9f2f53a3fd31634.png)  
Powerpoint uses several plugins.  
  
These can be installed via invoegen/invoegtoepassingen/invoegtoepassingen downloaden.  
  
The next plugins are installed at this moment:  
  
- Pro Word Cloud  
  
- QR4Office  
  

## System Software : Jenkins automation server

  
    _figuur 17_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-18315fcaa7814544b525847d661c8eee.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-18315fcaa7814544b525847d661c8eee.png)  
Jenkins automation server is used to build the artifacts and realize the accessability of the artifacts. This is done by uploading the latest created artifacts via SFTP to the shared directory J:\ARTIFACTS-FROM-JENKINS. The mapping J:\ARTIFACTS-FROM-JENKINS acts as a software repository.

## System Software : VirtualBox virtualisation software

  
    _figuur 18_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-87b72cc2ab9342448cd6b6f463b77e1f.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-87b72cc2ab9342448cd6b6f463b77e1f.png)  

## System Software

  
    _figuur 19_ _file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-198186927b414fe7a1be46e91f6d12af.png_  
![](file:///M:/archi/Developer4IT 2022-09-09/HTML//f03f34bb-5b66-4d8f-ad58-3641f8867e23/images/id-198186927b414fe7a1be46e91f6d12af.png)  
Jenkins automation server is used to build the artifacts and realize the accessability of the artifacts. This is done by uploading the latest created artifacts via SFTP to the shared directory J:\ARTIFACTS-FROM-JENKINS. The mapping J:\ARTIFACTS-FROM-JENKINS acts as a software repository.

  
  

f03f34bb-5b66-4d8f-ad58-3641f8867e23